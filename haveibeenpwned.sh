#!/usr/bin/env bash
# Copyright (c) 2018-2023 thomas.zink _at_ uni-konstanz _dot_ de
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

_usage="usage: $(basename "$0") <path/to/haveibeenpwned-mails>"
usage() {
    echo "Extract breach and email addresses from haveibeenpwned notification mails."
    echo "${_usage}"
}

# check directory
[ -z "$1" ] && { usage; exit 1; }
indir="${1:-./}"
[ ! -d "${indir}" ] && { echo "Error: ${indir} not a directory"; usage; exit 1; }
indir="$(realpath "${indir}")"
#pdir="${indir}/haveibeenpwned"

cd "${indir}" || exit 1
#mkdir -vp "${pdir}" 1>&2

#PWNBASE="https://haveibeenpwned.com/PwnedWebsites#"

# format: date,breach,user
find "${indir}" -maxdepth 1 -type f -iname '*haveibeenpwned*.eml' | while read -r mail; do 
    echo "Info: parse ${mail}" 1>&2
    date=$(grep -i "^date:" "${mail}" | cut -d":" -f2 | xargs -I {} date -d {} +%Y-%m-%d)
    tag=$(grep -i "^subject:" "${mail}" | sed 's/.*haveibeenpwned.*[:|#|\ ]//I' | sed 's/[[:cntrl:]]//g' ) 
    grep "^[a-z,A-Z,0-9,.]*@[uni-konstanz.de|uni.kn]" "$mail" | while IFS=$'\r\n' read -r user; do
        clean=$(tr -cd '[:print:]' <<< "$user")
        echo "$date,$tag,$clean"
    done
    #mv -v "${mail}" "${pdir}" 1>&2
done
