#!/bin/sh
# Copyright (c) 2018 thomas.zink _at_ uni-konstanz _dot_ de
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

_usage="usage: $(basename "$0") <path/to/mails>"
usage() {
    printf "Parse account lock notification emails and output (ts, account).\n"
    printf '%s' "${_usage}"
}

[ -z "$1" ] && { usage; exit 1; }

# dir with notification emails
dir="${1:-./}"
[ ! -d "${dir}" ] && { echo "Error: ${dir} not a directory"; usage; exit 1; }
dir="$(realpath "${dir}")"

# where to move the processed mails
#pdir="${dir}/compromised-accounts/"
#mkdir -p "${pdir}"

# parse emails
find "$dir" -maxdepth 1 -type f -iname '*Konto*gesperrt*.eml' | while read -r f; do
    echo "Info: parse $f" 1>&2
    user=$(echo "$f" | sed 's/.*Konto[_\ ]\(.*\)[_\ ]gesperrt.*/\1/' | tr -d " ")
    date=$(grep "Date: " "$f" | cut -d":" -f2 | xargs -I {} date -d {} +%Y-%m-%d)
    sogo=""
    imap=""
    #mv -v "${f}" "${pdir}/" 1>&2
    echo "$date,$user,$sogo,$imap"; 
done

