#!/bin/sh
# Copyright (c) 2018-2023 thomas.zink _at_ uni-konstanz _dot_ de
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
#
# shellcheck disable=SC2034  # Unused variables left for readability

usage() {
    printf "%s </path/to/csvfile>\n" "$(basename "$0")"
    printf "read csv file and reverse lookup hostnames\n"
    printf "\tcsvfile: file with fields 'timestamp', 'ip', 'tag', 'count', 'raw'\n"
}

if [ "$#" -lt 1 ]; then
    usage;
    exit 0;
fi
infile="$1"

set -euf #-o pipefail

{
read -r
echo "ip, host, tag"
while IFS="," read -r ts ip tag count raw; do
    ip=$(echo "$ip" | tr -d "'" | tr -d " ");
    tag=$(echo "$tag" | tr -d "'" | tr -d "/" | tr -d " ");
    host=$(dig +noall +answer -x "${ip}" | awk '{print $5}')
	echo "$ip, $host, $tag" | tr -d "'"
done
} < "$infile"
