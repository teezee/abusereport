#!/usr/bin/env python
# -*- coding: utf-8 -*-
###
# Copyright (c) 2023 tzink _at_ htwg-konstanz _dot_ de
# Usage of the works is permitted provided that this instrument is retained
# with the works, so that any entity that uses the works is notified of this
# instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###
import argparse
import csv
import sqlite3
import os, sys

def parse_arguments():
    parser = argparse.ArgumentParser(description="Read CSV file with IoCs and write to SQLite3 database.")
    parser.add_argument("-d", "--db", required=True, help="Path to the SQLite database")
    parser.add_argument("-f", "--file", required=True, help="Path to the CSV file")
    parser.add_argument("--delim", default=';', required=False, help="Delimiter, default: ';'")
    args = parser.parse_args()
    
    if not os.path.exists(args.file):
        print(f"Error: The CSV file '{args.file}' does not exist.", file=sys.stderr)
        sys.exit(1)

    return args

def create_ioc_table(conn):
    cursor = conn.cursor()

    # Create the ioc table if it does not exist
    cursor.execute('''
        CREATE TABLE IF NOT EXISTS ioc (
            type TEXT,
            value TEXT PRIMARY KEY,
            category TEXT,
            comment TEXT,
            attribute_tag TEXT,
            event_info TEXT,
            event_tag TEXT
        )
    ''')
    conn.commit()

def read_csv_and_insert(conn, csv_file_path, delim):
    with open(csv_file_path, newline='', encoding='utf-8') as csv_file:
        csv_reader = csv.DictReader(csv_file, delimiter=delim)

        for row in csv_reader:
            cursor = conn.cursor()
            cursor.execute('''
                INSERT OR REPLACE INTO ioc (type, value, category, comment, attribute_tag, event_info, event_tag)
                VALUES (?, ?, ?, ?, ?, ?, ?)
            ''', (row['type'], row['value'], row['category'], row['comment'], row['attribute_tag'],
                  row['event_info'], row['event_tag']))
            conn.commit()
            print(f"Info: inserted {row['type']}, {row['value']}, {row['category']}, {row['comment']}, {row['attribute_tag']}, {row['event_info']}, {row['event_tag']}")

def main():
    args = parse_arguments()

    # Connect to the SQLite database
    conn = sqlite3.connect(args.db)

    # Create the ioc table if it does not exist
    create_ioc_table(conn)

    # Read the CSV file and insert/update records in the ioc table
    read_csv_and_insert(conn, args.file, args.delim)

    # Close the database connection
    conn.close()

if __name__ == "__main__":
    main()

