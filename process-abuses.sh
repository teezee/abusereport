#!/bin/bash
# Copyright (c) 2018-2023 thomas.zink _at_ uni-konstanz _dot_ de
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

#set -eu -o pipefail
#shopt -s failglob inherit_errexit

ME=$(basename "$(readlink -f "$0")")
MYPATH=$(dirname "$(readlink -f "$0")")

tmp="/tmp/${ME}-$(( 10000 + RANDOM % 100000 ))"
procdir="processed_abuses"
result="abuses.csv"


download_shadowserver() {
    if [[ -n $(command -v download-shadowserver.sh) ]]; then
        echo "Warning: download_shadowserver.sh not found. Ignoring digests"
        return
    fi
    ./download-shadowserver.sh "$indir"
}

preprocess_reports() {
	# find shadowserver and dfn-cert reports and move to tmp
	find "${indir}" -maxdepth 1 -type f -regextype sed -regex ".*[0-9\-]\{11\}.*\.csv" -exec mv -t "${tmp}" {} \+
	find "${indir}" -maxdepth 1 -type f -regextype sed -regex ".*[0-9\-]\{13\}.*\.xml" -exec mv -t "${tmp}" {} \+
	find "${indir}" -maxdepth 1 -type f -name \*.csv -exec mv -t "${tmp}" {} \+
}

process_reports() {
	# import to db
	"${MYPATH}"/import-abuses.py -c "${tmp}" -s "${tmp}" -d "${db}" >> "${output}"
	
	# move processed reports
	mkdir -vp "${outdir}/${procdir}"
	mv -v "${tmp}"/* "${outdir}/${procdir}"
}

find_contacts() {
	"${MYPATH}"/find-contact.py -c "${contacts}" -i "${output}" >> "${outdir}/to-be-notified.csv"
}

usage() {
	printf "usage: %s <-i indir>  [-o outdir] [-d /path/to/database] [-c /path/to/contacts]\n" "${ME}";
	printf "Process shadowserver digest emails, reports and dfn-cert reports.\n"
	printf "options:\n"
	printf "\t-i indir:\t input directory with digest emails and reports\n"
	printf "\t-o outdir:\t output directory for processed reports and result (default pwd)\n"
	printf "\t-d database:\t path to database file\n"
	printf "\t-c contact:\t path to contacts file ('.csv')\n"
	exit 1;
}

# parse options
indir="${PWD}"
outdir="${PWD}"
output=""
db=""
contacts=""
while getopts ":hi:o:d:c:" opt; do
	case "${opt}" in
		i)  [ ! -d "${OPTARG}" ] && { echo "ERROR: ${OPTARG} not a directory"; usage; }
			indir=$(readlink -f "${OPTARG}")
			;;
		o)  [ ! -d "${OPTARG}" ] && { echo "ERROR: ${OPTARG} not a directory"; usage; }
			outdir=$(readlink -f "${OPTARG}")
			;;
		d)  [ ! -f "${OPTARG}" ] &&  { echo "ERROR: ${OPTARG} not a file"; usage; }
			#db="-d $(readlink -f "${OPTARG}")"
			db=$(readlink -f "${OPTARG}")
			;;
		c)	[ ! -f "${OPTARG}" ] &&  { echo "ERROR: ${OPTARG} not a file"; usage; }
			contacts=$(readlink -f "${OPTARG}")
			;;
		h)	usage;;
		*)	usage;;
	esac
done
shift $((OPTIND-1))

[ ! -d "${indir}" ] && { echo "ERROR: missing input directory"; usage; }
output="${outdir}/${result}"

tmp="${indir}/tmp"
bak="${indir}/bak"

mkdir -vp "${tmp}"
mkdir -vp "${bak}"
cp -v "${indir}"/* "${bak}"/
parse_shadowserver_digests
preprocess_reports
process_reports
[ -n "${contacts}" ] && find_contacts
rmdir -v "${tmp}"
