#!/usr/bin/env bash
# Copyright (c) 2018-2023 thomas.zink _at_ uni-konstanz _dot_ de
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

_usage="usage: $(basename "$0") <path/to/dfn-credentials-mails>"
usage() {
    echo "Extract credential information from DFN credential notification mails."
    echo "${_usage}"
}

# check directory
[ -z "$1" ] && { usage; exit 1; }
indir="${1:-./}"
[ ! -d "${indir}" ] && { echo "Error: ${indir} not a directory"; usage; exit 1; }
indir="$(realpath "${indir}")"
#pdir="${indir}/dfn-credentials"

cd "${indir}" || exit 1
#mkdir -vp "${pdir}" 1>&2

# parse files and output credential list
# format: date,tag,user, pass 
find "${indir}" -maxdepth 1 -type f -iname 'DFN*Credentials*.eml' | while read -r mail; do 
    echo "Info: parse ${mail}" 1>&2
    date=$(grep -i "^date:" "${mail}" | cut -d":" -f2 | xargs -I {} date -d {} +%Y-%m-%d)
    tag=$(grep -i "^subject:" "${mail}" | sed 's/.*#\([[:digit:]]*-[[:digit:]]*\)\ \-.*/\1/') 
    grep "^[a-z,A-Z,0-9,.]*@uni-konstanz.de" "$mail" | while IFS=$'\n' read -r line; do
        readarray -d "|" -t userpass <<< "$line" 
        printf "%s,%s,%s,%s" "$date" "$tag" "${userpass[0]}" "${userpass[1]}"
    done
    #mv -v "${mail}" "${pdir}" 1>&2
done
