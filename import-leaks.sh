#!/usr/bin/env bash
#shellcheck disable=SC1090
# Copyright (c) 2018-2023 thomas.zink _at_ uni-konstanz _dot_ de
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

_usage="usage: $(basename "$0") <tablename> <path/to/compromised-accounts.csv> [path/to/database]"
usage() {
    echo "Import leak data (accounts, pwned) into database."
    echo "${_usage}"
}

# load config if available
config_home="${XDG_CONFIG_HOME:-$HOME/.config}/cert/"
config="${config_home}/cert.conf"
[[ -f "$config" ]] && . "$config"

# check arguments
if [ "$#" -lt 2 ]; then
    usage;
    exit 0;
fi

# default to db in config
db=""
[[ -n "${ABUSEDB}" ]] && db="${ABUSEDB}"
[[ -n "${3}" ]] && db="${3}"
[[ ! -f "${db}" ]] && { echo "Error: ${db} not found"; usage; exit 1; }

[ -z "$1" ] && { usage; exit 1; }
table="${1}"

[ -z "${2}" ] && { usage; exit 1; }
data="${2}"
[ ! -f "${data}" ] && { echo "Error: ${data} not found"; usage; exit 1; }

# import data to database
sqlite3 "${db}" -cmd ".mode csv" ".import ${data} ${table}"

