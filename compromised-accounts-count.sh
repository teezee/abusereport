#!/bin/bash
# Copyright (c) 2018-2023 thomas.zink _at_ uni-konstanz _dot_ de
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
#
# read the compromised account csv and count number of accounts by month

usage() {
    printf "usage: %s <path/to/file>\n" "$(basename "$0")"
    printf "read csv file, count num/month, print hist\n"
    printf "\tfile: csv with 'date','account'\n"
}

if [ "$#" -lt 1 ]; then 
    usage;
    exit 0;
fi
accounts="$1"

months="$(cut -d'-' -f1-2 "${accounts}" | sort -u)"
for i in ${months}; do 
    count=$(grep -c "$i" "${accounts}");
    len=${#count}
    #off=$((3-${len}))
    off=$((3-len))
    printf "%s, " "$i"
    #for j in $(seq 1 "${off}"); do printf " "; done
    for (( c=0; c<"${off}"; c+=1 )); do printf " "; done
    printf "%s, " "$count"
    #for j in $(seq 1 "${count}"); do printf "▆"; done
    for (( c=0; c<"${count}"; c+=1 )); do printf " "; done
    printf "\n"
done
