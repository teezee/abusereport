# -*- coding: utf-8 -*-
###
# Copyright (c) 2018 thomas.zink _at_ uni-konstanz _dot_ de
# Usage of the works is permitted provided that this instrument is retained with the works,
# so that any entity that uses the works is notified of this instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###

import os
import csv
from lib.table import Table, NetworkSchema

__all__ = ['shadowserver_tablename', 'read_shadowserver']

try:
    import dateutil.parser as dateparse
except (ImportError, ModuleNotFoundError) as e:
    print (e)
    import pip
    pip.main(['install', '--user', 'python-dateutil'])
    import dateutil.parser as dateparse


def shadowserver_tablename(path: str) -> str:
    #return os.path.splitext(os.path.basename(path))[0].split('_', 1)[1]
    return os.path.splitext(os.path.basename(path))[0].split('-', 3)[-1]


def read_shadowserver(files: list) -> list:
    tables = {}
    for fil in files:
        with open(fil) as handle:
            reader = csv.reader(handle)
            tablename = shadowserver_tablename(handle.name)
            header = next(reader)
            schema = NetworkSchema.fromheader(header)
            try:
                tab = tables[tablename]
            except KeyError:
                tab = Table(tablename, schema)
                tables[tablename] = tab
            for row in reader:
                tab.add(row)
    return tables.values()
