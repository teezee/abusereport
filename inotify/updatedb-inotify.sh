#!/usr/bin/env bash
command -v inotifywait >/dev/null 2>&1 || { echo >&2 "ERROR: inotifywait required (inotify-tools)."; usage; exit 1; }

BIN="./"
WATCH="./sorted"
DB="./db/abuse.db"

mkdir -p "${WATCH}" ./db

inotifywait --monitor -r -e close_write -e moved_to --format '%e %w %f' ${WATCH} |
while read -r event watch file; do
	(
	echo "${event} in ${watch} on file ${file}"
	if [ ${file: -4} == ".csv" ]; then
		"${BIN}"/import-abuses.py -s "${watch}/${file}" -d "${DB}"
	elif [ ${file: -4} == ".xml" ]; then
		"${BIN}"/import-abuses.py -c "${watch}/${file}" -d "${DB}"
	fi
	)  &
done
