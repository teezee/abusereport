#!/usr/bin/env python3
# -*- coding: utf-8 -*-
###
# Copyright (c) 2023 tzink _at_ htwg-konstanz _dot_ de
# Usage of the works is permitted provided that this instrument is retained
# with the works, so that any entity that uses the works is notified of this
# instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###
import argparse
import sqlite3
from datetime import datetime
import sys
import ipaddress

def parse_arguments():
    parser = argparse.ArgumentParser(description="Add records to the incidents table.")
    parser.add_argument("-d", "--db", required=True, help="Path to the SQLite database")
    parser.add_argument("-r", "--record", type=str, help="Whole record as comma-separated values")
    parser.add_argument("-t", "--tag", type=str, help="Tag for the record")
    parser.add_argument("-i", "--ip", type=str, help="IP address for the record")
    parser.add_argument("--ts", type=str, help="Timestamp for the record")
    parser.add_argument("--raw", type=str, help="Raw data for the record")
    parser.add_argument("--count", type=int, help="Counter for the record")
    args = parser.parse_args()
    
    if not args.record and (not all([args.tag, args.ip, args.ts, args.raw, args.count])):
        print("Error: Either --record or individual fields (tag, ip, ts, raw, count) must be provided.")
        sys.exit(1)
    
    return args

def parse_record(args):
    record = dict()

    if args.record:
        ts, ip, tag, count, raw = map(str.strip, args.record.split(','))
    else:
        ts, ip, tag, count, raw = args.ts, args.ip, args.tag, args.count, args.raw

    try:
        record['ts'] = datetime.strptime(ts, '%Y-%m-%d %H:%M:%S')
    except ValueError:
        raise ValueError("Invalid timestamp format. Please use '%Y-%m-%d %H:%M:%S'.")

    try:
        record['ip'] = ipaddress.ip_address(ip)
    except ValueError:
        raise ValueError("Invalid IP address.")

    try:
       record['tag'] = str(tag)
    except ValueError:
        raise ValueError("Tag must be a string.")

    try:
        record['count'] = int(count)
    except ValueError:
        raise ValueError("Count must be an integer.")

    try:
        record['raw'] = str(raw)
    except ValueError:
        raise ValueError("Tag must be a string.")

    return record 

def create_table(conn):
    cursor = conn.cursor()

    # Create the incidents table if it does not exist
    cursor.execute('''
        CREATE TABLE IF NOT EXISTS incidents (
            ts_first TEXT,
            ts_last TEXT,
            tag TEXT,
            count TEXT,
            description TEXT,
            PRIMARY KEY ('ts_first', 'tag')
        )
    ''')
    conn.commit()

def insert_or_update_record(conn, record):
    cursor = conn.cursor()

    ts, ip, tag, count, raw = str(record['ts']), str(record['ip']), record['tag'], record['count'], record['raw']

    # Check if a record with the given ('ip', 'tag') key already exists
    cursor.execute("SELECT * FROM abuses WHERE ip = ? AND tag = ?", (ip, tag))
    existing_record = cursor.fetchone()

    if existing_record:
        # Update the existing record
        #updated_timestamp = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')
        updated_timestamp = datetime.strptime(ts, '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%d %H:%M:%S')
        updated_count = int(existing_record[3]) + int(count)

        cursor.execute("UPDATE abuses SET timestamp = ?, count = ? WHERE ip = ? AND tag = ?",
                       (updated_timestamp, updated_count, ip, tag))
    else:
        # Insert a new record
        cursor.execute("INSERT INTO abuses (timestamp, ip, tag, count, raw) VALUES (?, ?, ?, ?, ?)",
                       (ts, ip, tag, count, raw))

    conn.commit()


def main():
    args = parse_arguments()
    record = parse_record(args)
    conn = sqlite3.connect(args.db)
    create_table(conn)
    insert_or_update_record(conn, record)
    conn.close()

if __name__ == "__main__":
    main()

