#!/usr/bin/env python3
# vim: ts=4 sw=4 sts=4 noet :
# Copyright (c) 2018-2023 thomas.zink _at_ uni-konstanz _dot_ de
# Usage of the works is permitted provided that this instrument is retained
# with the works, so that any entity that uses the works is notified of this
# instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

import ipaddress
import socket
import csv
import sys
import os
import getopt
from xdg import BaseDirectory


def usage():
	print(sys.argv[0] + " [-n] <-c contacts> <-i ip|txt|csv> [-f field]")
	print("Find network and contact for ip address.")
	print("\t-c contacts: path to contact input file (.csv)")
	print("\t-i ip: ip address, or list of ips in '.txt', or 'csv' with 'ip' field")
	print("\t-n: don't do name resolution")
	print("\tfield: print only matching 'field' of the contacts.csv. Default: print whole line")
	sys.exit(2)


def read_config():
	contacts = None
	config = str(BaseDirectory.xdg_config_home)+"/cert/cert.conf"
	if os.path.isfile(config):
		handle = open(config)
		fil = handle.readlines()
		handle.close()
		d = {}
		for line in fil:
			try:
				k, v = line.strip().split('=')
				d[k] = v.strip('"')
			except ValueError:
				pass
		contacts = d["CONTACTS"].replace("$HOME", os.environ["HOME"]).replace("${HOME}", os.environ["HOME"])
	return contacts


def todict(keys: list, values: list) -> dict:
	return dict(zip([k.strip() for k in keys], [v.strip() for v in values]))


def ascsv(it: [], delim=",") ->str:
	return delim.join(list(it))


def astxt(it: []) ->str:
	return str(it)


def readcsv(fname: str, delim: str = ',') -> list:
	handle = open(fname, 'r')
	reader = csv.reader(handle, delimiter=delim)
	header = next(reader)
	records = []
	for row in reader:
		records.append(todict(header, row))
	handle.close()
	return records


def readtxt(fname: str) -> list:
	handle = open(fname, 'r')
	ips = []
	for line in handle.readlines():
		ips.append(line.strip())
	handle.close
	return ips


def readstdin(fname: str) -> []:
	ips = []
	for line in sys.stdin:
		ips.append(line.strip())
	return ips


def longest_prefix_match(records: list, ip: str, nodns: bool = False) -> dict:
	try:
		ip = ipaddress.ip_address(ip)
	except ipaddress.AddressValueError as e:
		raise Exception("AddressValueError: %s. '%s' is no ipaddress" %(e, ip), file=sys.stderr)
	host=""
	if not nodns:
		try:
			host = socket.gethostbyaddr(str(ip))[0]
		except socket.herror as e:
			print("socket.herror: %s on ip %s" %(e, ip), file=sys.stderr)
	longest_prefix = 0
	found = dict(zip([k for k in records[0]], [""]*len(records[0])))
	for r in records:
		if r['net']:
			net = ipaddress.ip_network(r['net'], strict=False)
			if ip in net and net.prefixlen > longest_prefix:
				found = r
				longest_prefix = net.prefixlen
	return {'ip': str(ip), 'host': str(host), **found }


def optarg(argv: []) -> object:
	db = None
	ip = None
	isfile = False
	field = None
	nodns = False

	try:
		opts, args = getopt.getopt(argv, "hc:i:f:n", ["help", "contacts=", "ip="])
	except getopt.GetoptError:
		usage()

	for opt, arg in opts:
		if opt in ("-h", "--help"):
			usage()
		elif opt in ("-c", "--contacts"):
			if not os.path.isfile(arg):
				print("ERROR: '%s' not found" % arg, file=sys.stderr)
				usage
			db = arg
		elif opt in ("-i", "--ip"):
			if os.path.isfile(arg):
				if not (arg.endswith('.txt') or arg.endswith('.csv')):
					print("ERROR: %s is not .txt or .csv", file=sys.stderr)
					usage
				ip=arg
				isfile=os.path.splitext(arg)[-1].lower()	
			else:
				try:
					#ip = ipaddress.IPv4Address(arg)
					ip = ipaddress.ip_address(arg)
				except ipaddress.AddressValueError as e:
					print("AddressValueError: %s. %s not a valid ipaddress" % (e, arg), file=sys.stderr)
					usage
		elif opt in ("-f"):
			field=str(arg)
		elif opt in ("-n"):
			nodns=True

	# if not db or not ip:
	if not db:
		db = read_config()
		if not db:
			usage()

	if not ip:
		print("INFO: read ips from stdin", file=sys.stderr)
		ip=sys.stdin
		isfile='stdin'

	return type("optargs", (object,), {'db': db, 'ip': ip, 'isfile': isfile, 'field': field, 'nodns': nodns})


def main(argv: []) -> []:
	opts = optarg(argv)
	delimiter = ','
	if opts.db.endswith('.psv'):
		delimiter = '|'
	contacts = readcsv(opts.db, delimiter)
	if opts.isfile == '.csv':
		ips = [x['ip'] for x in readcsv(opts.ip, delim=',')]
	elif opts.isfile == '.psv':
		ips = [x['ip'] for x in readcsv(opts.ip, delim='|')]
	elif opts.isfile == '.txt':
		ips = readtxt(opts.ip)
	elif opts.isfile == 'stdin':
		ips = readstdin(opts.ip)
	else:
		ips = [opts.ip]

	matches = []
	header = None
	for ip in ips:
		try:
			found = longest_prefix_match(contacts, ip, opts.nodns)
			if not header:
				header = found.keys()
			if not opts.field:
				# print(header)
				print(ascsv(list(found.values()), delimiter))
				# print(astxt(found.values()))
			else:
				print(found[opts.field])
			matches.append(found)
		except Exception as e:
			print("%s" %e, file = sys.stderr)
			pass
	return matches


if __name__ == '__main__':
	found = main(sys.argv[1:])
	#print("%s" %(found))

