#!/usr/bin/env sh
# Copyright (c) 2018-2023 thomas.zink _at_ uni-konstanz _dot_ de
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

_usage="usage: $(basename "$0") <path/to/shadowserver-mails>"
usage() {
    echo "Download shadowserver reports from digest mails."
    echo "${_usage}"
}

# requires: rename (debian) or perl-rename (arch)
if [ -n "$(command -v pacman)" ]; then
    rename=$(command -v perl-rename)
fi

if [ -n "$(command -v apt)" ]; then
    rename=$(command -v rename)
fi

if [ -z "${rename}" ]; then
    echo "Error: command rename not found." 1>&2
    echo "arch: pacman -Su perl-rename" 1>&2
    echo "debian: apt install rename" 1>&2
    exit 1
fi

if [ -z "$(command -v urlscan)" ]; then
    echo "Info: command urlscan not found, rfc822 mails not supported." 1>&2
    echo "Info: install urlscan to fix" 1>&2
fi

echo "Info: using rename=${rename}" 1>&2 

# check directory
[ -z "$1" ] && { usage; exit 1; }
indir="${1:-./}"
[ ! -d "${indir}" ] && { echo "Error: ${indir} not a directory"; usage; exit 1; }
indir="$(realpath "${indir}")"
#pdir="${indir}/digests"

cd "${indir}" || exit 1
#mkdir -p "${pdir}"

# rename malformed eml
#$rename -v 's/,//g' "${indir}"/*.eml 1>&2
#$rename -v 's/\ /_/g' "${indir}"/*.eml 1>&2

# parse files and download reports
find "${indir}" -maxdepth 1 -type f -iname '*Digest*.eml' | while read -r mail; do
    echo "parse ${mail}"
    mime=$(file -bi "${mail}" | cut -d";" -f1)
    if [ "${mime}" = "message/rfc822" ]; then
        if [ -z "$(command -v urlscan)" ]; then
            echo "Warning: urlscan not found. rfc822 messages not supported. continue." 1>&2
            continue
        fi
        for url in $(urlscan -cn "${mail}" | grep -sh dl.shadowserver.org); do
            echo "curl ${url}"
            curl -s -J -O "${url}"
        done
        #mv -v "${mail}" "${pdir}"
    elif [ "${mime}" = "text/plain" ]; then
        grep -sh dl.shadowserver.org "${mail}"| while read -r url; do 
            echo "curl ${url}"
            curl -s -J -O "${url}"
        done
        #mv -v "${mail}" "${pdir}"
    else
        echo "Error: unsupported mime type ${mime}. continue" 1>&2
        continue
    fi
done

# rename malformed reports
$rename -v 's/-universitt_konstanz-ip//g' "${indir}"/*.csv 1>&2
