#!/bin/bash
# Copyright (c) 2018 thomas.zink _at_ uni-konstanz _dot_ de
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
# find new entries in dfn-credential data.csv

usage() {
    printf "usage: %s </path/to/data> </path/to/reference>\n" "$(basename "$0")"
    printf "find new,  valid entries entries in dfn credentials\n"
    printf "\tdata: dfn credential data.csv ('user'|'pass')\n"
    printf "\treference: csv with previous data ('user'|'pass')\n"
}

if [ "$#" -lt 2 ]; then 
    usage;
    exit 0;
fi
data="$1"
db="$2"
new="data-new.csv"

#known="data-known.csv"
#while read -r i; do grep -i "$i" "${db}" >> "${known}"; done < "${data}"
#grep -Eiwv "$(tr '\n' '\|' < ${known})" "${data}" >> "${new}"

comm -3 <(sort -u "$data") <(sort -u "$db") > "$new"
