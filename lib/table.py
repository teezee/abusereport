# -*- coding: utf-8 -*-
###
# Copyright (c) 2020 thomas.zink _at_ uni-konstanz _dot_ de
# Usage of the works is permitted provided that this instrument is retained with the works,
# so that any entity that uses the works is notified of this instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###

import sys
import os
import csv
import sqlite3
from datetime import datetime
import pytz
import ipaddress

try:
    from configobj import ConfigObj as confobj
except ImportError:
    print("ERROR: package 'configobj' required. Trying to install ...")
    import pip
    pip.main(['install', '--user', 'configobj'])
    from configobj import ConfigObj as confobj

try:
    import dateutil.parser as dateparse
except (ImportError, ModuleNotFoundError) as e:
    print (e)
    import pip
    pip.main(['install', '--user', 'python-dateutil'])
    import dateutil.parser as dateparse

"""
version independent representations of an IP address and network
"""
class IPaddress(ipaddress._IPAddressBase):
    def __init__(self, ip: str):
        try:
            self._ip = ipaddress.ip_address(ip)
        except ValueError:
            print('address invalid for IP:', ip)
  
    def _string_from_ip_int(cls, ip_int):
        return self._ip._string_from_ip_int(self._ip)

    def __str__(self):
        return self._ip.__str__()


class IPnetwork(ipaddress._BaseNetwork):
    def __init__(self, net: str):
        try:
            network = ipaddress.ip_network(net)
            self.__dict__ = dict(network)
        except ValueError:
            print('network invalid for IP:', net)


"""
a schema is a dictionary of identifiers and types
{ name(str): ( index(int), type ) }
"""
class Schema(dict):
    #__sqldatatypes = {str: "TEXT", int: "INTEGER", datetime: "TEXT", ipaddress.IPv4Address: "TEXT", ipaddress.IPv4Network: "TEXT", float: "REAL"}
    __sqldatatypes = {str: "TEXT", int: "INTEGER", datetime: "TEXT", IPaddress: "TEXT", IPnetwork: "TEXT", float: "REAL"}

    def __init__(self, kwargs):
        dict.__init__(self, kwargs)
        self.primarykey = None

    @staticmethod
    def fromheader(header):
        return Schema({x: (i, str) for i, x in enumerate(header)})

    def correcttypes(self, iterable):
        errors = []
        for idx, typ in self.values():
            if not isinstance(iterable[idx], typ):
                try:
                    typ(iterable[idx])
                #except Exception as e:
                except (IndexError, TypeError) as e:
                    errors.append(idx)
        return errors

    def correctlen(self, iterable):
        return len(iterable) - len(self)

    def correct(self, iterable):
        return (self.correcttypes(iterable)==[]) & (self.correctlen(iterable)==0)

    @property
    def header(self) -> list:
        return sorted(list(self.keys()), key=lambda x: self[x][0])

    @property
    def types(self) -> list:
        return [v[1] for v in sorted(self.values())]

    def indexof(self, field: str) -> int:
        return self[field][0] if field in self.keys() else None

    def typeof(self, field: str) -> type:
        return self[field][1] if field in self.keys() else None

    def datatype(self, field: str) -> str:
        typ = "TEXT"
        if field in self.__sqldatatypes.keys():
            typ = self.__sqldatatypes[field]
        return typ


class NetworkSchema(Schema):
    @staticmethod
    def fromheader(header):
        d = {}
        for i in range(len(header)):
            field = header[i]
            if field == 'timestamp' or field == 'ts':
                typ = datetime
            elif field == 'ip' or field == 'ipaddress' or field == 'src_ip' or field == 'dst_ip':
                #typ = ipaddress.IPv4Address
                typ = IPaddress
            elif field == 'net' or field == 'network':
                #typ = ipaddress.IPv4Network
                typ = IPnetwork
            elif field == 'port' or field == 'src_port' or field == 'dst_port':
                typ = int
            else:
                typ = str
            d[field] = (i, typ)
            #print("[%s] = %s" %(field,typ))
        return NetworkSchema(d)

    def correcttypes(self, iterable):
        errors = []
        for k, v in self.items():
            idx, typ = v
            if not isinstance(iterable[idx], typ):
                try:
                    if k == 'timestamp' or k == 'ts':
                        dateparse.isoparse(iterable[idx]).replace(tzinfo=pytz.utc).isoformat()
                    else:
                        typ(iterable[idx])
                except (IndexError, TypeError) as e:
                    errors.append(idx)
        return errors


"""
class Table
A table is a set of tuples according to a schema and identifiable by name.
"""
class Table(set):
    def __init__(self, name: str, schema: Schema, iterable=None):
        super().__init__(self)
        self.__name = name
        self.__schema = schema
        if iterable:
            self.update(self, iterable)

    def __assertrecord (self, iterable):
        pass
        # diff = self.__schema.correctlen(iterable)
        # typerr = self.__schema.correcttypes(iterable)
        # if not diff == 0:
            # raise IndexError("record %s differs in length (exp %d, len %d)\nschema: %s" %(iterable, len(self.__schema), len(iterable),self.__schema.keys()))
        # if not typerr == []:  
            # raise TypeError("record types differ in indexes %s" % (typerr))

    def __createrecord (self, iterable):
        try:
            self.__assertrecord(iterable)
        except (IndexError, TypeError) as e:
            print("ERROR: %s, record not created (%s)" %(e,iterable), file=sys.stderr)
            return None
        return tuple(iterable)

    def add(self, element: object) -> None:
        record = self.__createrecord(element)
        if record:
            set.add(self, record)

    def update(self, *elements) -> None:
        for e in elements:
            self.add(e)

    @property
    def name(self):
        return self.__name

    @property
    def schema(self):
        return self.__schema

    @property
    def header(self):
        return self.__schema.header

    @property
    def primarykey(self):
        return self.__schema.primarykey

    @property
    def records(self):
        return list(self)


# table conversions ##############################################################################


def table2sql(table: Table) -> list:
    sqllist = []
    if table.primarykey:
        create_table_sql = "CREATE TABLE IF NOT EXISTS %s (%s, PRIMARY KEY (%s));" % (table.name, str([k + " " + table.schema.datatype(k) for k in table.header])[1:-1].replace("'",""), str(table.primarykey)[1:-1])
    else:
        create_table_sql = "CREATE TABLE IF NOT EXISTS %s (%s);" % (table.name, str([k + " " + table.schema.datatype(k) for k in table.header])[1:-1].replace("'",""))
    sqllist.append(create_table_sql)
    for rec in table.records:
        insert_sql = "REPLACE INTO %s VALUES %s;" %(table.name, rec)
        sqllist.append(insert_sql)
    return sqllist


def table2csv(table: Table) -> list:
    #csvlist = [ str(table.header)[1:-1].replace("'", "").replace(", ", ",") ]
    csvlist = []
    for row in table:
        csvlist.append(str(row)[1:-1].replace("'", "").replace(", ", ","))
    return csvlist


def write_csv(table: Table, path):
    if not os.path.isdir(path):
        raise FileNotFoundError("ERROR: %s not found." % (path))
    fname = os.path.join(path, "%s.csv" % (table.name))
    with open(fname, 'w') as fil:
        writer = csv.writer(fil, delimiter=',', quotechar='"')
        #lines = table2csv(table)
        writer.writerow(table.header)
        for row in table:
            writer.writerow(row)


def table2cfg(table: Table) -> list:
    cfglist = []
    for rec in table.records:
        cdict = { k: rec[v[0]] for k, v in table.schema.items()}
        cfglist.append(confobj(cdict))
    return cfglist


def csv2table(fname: str, tablename: str = None, schema: type = Schema) -> Table:
    handle = open(fname)
    reader = csv.reader(handle)
    if not tablename:
        tablename = os.path.splitext(os.path.basename(fname))[0]
    header = next(reader)
    tab = Table(tablename, schema.fromheader(header))
    for row in reader:
        tab.add([field.strip() for field in row])
    handle.close()
    return tab


def cfg2table(fname: str, tablename: str = None, schema: type = Schema) -> Table:
    cdict = dict(confobj(fname))
    if not tablename:
        tablename = os.path.splitext(os.path.basename(fname))[0]
    tab = Table(tablename, schema.fromheader(list(cdict.keys())))
    tab.add([field.strip() for field in cdict.values()])
    return tab


def executesql(db: sqlite3.Connection, sqlscript: list) -> None:
    for statement in sqlscript:
        try:
            db.execute(statement)
        except sqlite3.OperationalError as e:
            print ("ERROR Executing '%s': %s." % (statement, e), file=sys.stderr)
    db.commit()
