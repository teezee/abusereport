#!/usr/bin/env bash
# shellcheck disable=SC1090
# Copyright (c) 2018-2023 thomas.zink _at_ uni-konstanz _dot_ de
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

_usage="usage: $(basename "$0") <path/to/compromised-accounts.csv> [path/to/database]"
usage() {
    echo "Import compromised accounts into database."
    echo "${_usage}"
}

# load config if available
config_home="${XDG_CONFIG_HOME:-$HOME/.config}/cert/"
config="${config_home}/cert.conf"
[[ -f "$config" ]] && . "$config"

# check arguments
[ -z "$1" ] && { usage; exit 1; }
data="${1}"
[ ! -f "${data}" ] && { echo "Error: ${data} not found"; usage; exit 1; }

# default to db in config
[[ -n "${ABUSEDB}" ]] && db="${ABUSEDB}"
[[ -n "${2}" ]] && db="${2}"
[[ -n "${db}" ]] && [[ ! -f "${db}" ]] && { echo "Error: ${db} not found"; usage; exit 1; }

# import data to database
sqlite3 "${db}" -cmd ".mode csv" ".import ${data} accounts"

