# -*- coding: utf-8 -*-
###
# Copyright (c) 2018 thomas.zink _at_ uni-konstanz _dot_ de
# Usage of the works is permitted provided that this instrument is retained with the works,
# so that any entity that uses the works is notified of this instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###

from xml.etree import ElementTree as ET
from defusedxml import ElementTree as dET # for parsing
from lib.table import Table, NetworkSchema
import os


def read_dfncert(files):
    tables = {}
    for fil in files:
        root = dET.parse(fil).getroot()
        messages = list(root)
        for message in messages:
            events = []
            attrib = message.attrib.copy()
            for c in list(message):
                if "description" in c.tag:
                    attrib.update({'description': c.text})
                if "event" in c.tag:
                    attrib.update(c.attrib)
                    events.append(attrib.copy())
            tablename = attrib['category']
            header = ['timestamp', 'ip', 'category']
            header += sorted([x for x in attrib.keys() if x not in header])
            schema = NetworkSchema.fromheader(header)
            try:
                tab = tables[tablename]
            except KeyError:
                tab = Table(tablename, schema)
                tables[tablename] = tab
            for e in events:
                row = [e[x].replace('\n',' ') for x in header]
                #row = [e[x] for x in header]
                tab.add(row)
    return tables.values()


def table2dfnxml(table: Table) -> ET.ElementTree:
    messages = {}
    if len(table) > 0:
        for rec in table:
            ts = rec[table.schema.indexof('timestamp')]
            ip = str(rec[table.schema.indexof('ip')])
            mkey = (ts, ip, table.name)
            event = {}
            ekeys = [k for k in table.schema.keys() if k not in ['ip']]
            for k in ekeys:
                event[k] = rec[table.schema.indexof(k)]
            try:
                messages[mkey].append(event)
            except KeyError:
                messages[mkey] = [event]
    root = ET.Element("warning")
    for mkey in messages.keys():
        xmessage = ET.SubElement(root, "message")
        xmessage.set('timestamp', mkey[0])
        xmessage.set('ip', mkey[1])
        xmessage.set('category', mkey[2])
        events = messages[mkey]
        xmessage.set('count', str(len(events)))
        for event in events:
            xevent = ET.SubElement(xmessage, "event")
            for ekey in event:
                xevent.set(ekey, event[ekey])
    tree = ET.ElementTree(root)
    return tree


def write_dfnxml(table: Table, path):
    if os.path.isdir(path):
        fname = os.path.join(path, "%s.xml" % (table.name))
    elif os.path.isfile():
        fname = path
    else:
        raise FileNotFoundError("ERROR: %s not found." % (path))
    tree = table2dfnxml(table)
    tree.write(fname)
    return tree
