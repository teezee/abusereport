#!/usr/bin/env python3
# -*- coding: utf-8 -*-
###
# Copyright (c) 2020 thomas.zink _at_ uni-konstanz _dot_ de
# Usage of the works is permitted provided that this instrument is retained with the works,
# so that any entity that uses the works is notified of this instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###

import sys
import os
import getopt
import glob
from lib.table import *


def usage():
    print(sys.argv[0] + " <-p path> [-d database]")
    print("Parse .csv and .conf files and convert to db script.")
    print("\t-p|--path: path to input file(s)")
    print("\t-d|--db: path to sqlite3 database")
    sys.exit(2)


def optarg(argv, usage):
    suffixes = ['csv', 'conf']
    _files = None
    _db = None

    try:
        opts, args = getopt.getopt(argv, "hp:d:t:", ["help", "path=", "db=", "type="])
    except getopt.GetoptError:
        usage()

    for opt, arg in opts:
        if opt in ("-h", "--help"):
            usage()
        elif opt in ("-p", "--path"):
            if os.path.isdir(arg):
                _files = []
                for suf in suffixes:
                    _files.extend(glob.glob(os.path.join(arg, "") + "*." + suf))
            elif os.path.isfile(arg):
                _files = [arg]
            else:
                print("ERROR: '%s' not found" % arg, file=sys.stderr)
                usage()
        elif opt in ("-d", "--db"):
            if not os.path.isfile(arg):
                print("INFO: '%s' not found, creating new" % arg, file=sys.stderr)
            _db = arg
        elif opt in ("-t", "--type"):
            suffix = arg.strip(".")

    if _files == None:
        usage()

    return type("optargs", (object,), {'files': _files, 'db': _db})


def main(argv):
    opts = optarg(argv, usage)

    sqlscript = []
    for fil in opts.files:
        if fil.endswith('csv'):
            table = csv2table(fil)
        elif fil.endswith('conf'):
            table = cfg2table(fil)
        sqlscript.extend(table2sql(table))

    if opts.db:
        db_conn = sqlite3.connect(opts.db)
        executesql(db_conn, sqlscript)
        db_conn.close()

    return sqlscript


if __name__ == '__main__':
    sqlscript = main(sys.argv[1:])
    for statement in sqlscript:
        print(statement)