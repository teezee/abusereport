#!/bin/bash
# Copyright (c) 2018-2023 thomas.zink _at_ uni-konstanz _dot_ de
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

set -ef -o pipefail

usage() {
	echo "usage: $0 </path/to/abuse-db>"
	echo "find cname to ip and insert into database"
}

if [ -z "$1" ]; then usage; exit 1; fi
#[ -z "$1" ] && usage; exit 1;


DB="$1"

ips=$(sqlite3 "$DB" "SELECT ip from hosts;" | tr '\n' ' ')
for ip in $ips; do 
	cname=$(dig +noall +answer -x "$ip" | rev | cut -f1 | rev | tr '\n' ' '); 
	echo sqlite3 "$DB" "INSERT INTO hostnames (ip,cname) values (\"$ip\", \"$cname\");"
	sqlite3 "$DB" "INSERT INTO hostnames (ip,cname) values (\"$ip\", \"$cname\");"; 
done
